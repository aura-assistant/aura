package main

import (
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "aurad <subcommand>",
	Short: "Aura is a smart home assistant",
	Long: `
Aura is core of a home assistant.
More info at https://github.com/aryahadii/aura`,
	Run: nil,
}

func init() {
	cobra.OnInitialize()
}
