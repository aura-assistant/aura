package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/aura-assistant/aura"

	"github.com/sirupsen/logrus"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Version of Aura assistant",
	Run: func(cmd *cobra.Command, args []string) {
		logVersion()
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}

func logVersion() {
	logrus.Info("version   > ", aura.Version)
	logrus.Info("buildtime > ", aura.BuildTime)
	logrus.Info("commit    > ", aura.Commit)
}
